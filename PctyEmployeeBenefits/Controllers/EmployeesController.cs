﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PctyEmployeeBenefits.Data;
using PctyEmployeeBenefits.Models;
using PctyEmployeeBenefits.Models.Translators;

namespace PctyEmployeeBenefits.Controllers
{
	[ApiController, Route("api/[controller]")]
	public class EmployeesController : ControllerBase
	{
		private readonly BenefitsContext _context;
		private readonly IEmployeeTranslator _employeeTranslator;
		private readonly IDependentTranslator _dependentTranslator;

		public EmployeesController(BenefitsContext context, 
		IEmployeeTranslator employeeTranslator,
		IDependentTranslator dependentTranslator)
		{
			_context = context;
			_employeeTranslator = employeeTranslator;
			_dependentTranslator = dependentTranslator;
		}

		// GET: api/Employees
		[HttpGet]
		public async Task<ActionResult<List<EmployeeDTO>>> GetEmployees()
		{
			return await _context.Employees
				.Include(empl => empl.Dependents)
				.ThenInclude(dep => dep.Relationship)
				.Select(empl => _employeeTranslator.toDTO(empl, _dependentTranslator))
				.ToListAsync();
		}

		// GET: api/Employees/3
		[HttpGet("{id}", Name = "GetEmployee")]
		public async Task<ActionResult<EmployeeDTO>> GetEmployee(int id)
		{
			var employee = await _context.Employees
			.Include(empl => empl.Dependents)
			.ThenInclude(dep => dep.Relationship)
			.FirstOrDefaultAsync(empl => empl.Id == id);

			if (employee == null)
			{
			return NotFound();
			}

			return _employeeTranslator.toDTO(employee, _dependentTranslator);
		}

		// PUT: api/Employees/3
		[HttpPut("{id}")]
		public async Task<IActionResult> PutEmployee(int id, EmployeeDTO dto)
		{
			if (id != dto.Id)
			{
				return BadRequest();
			}

			Employee employee = await _context.Employees.FindAsync(id);
			employee.Update(dto.FirstName, dto.LastName);

			_context.Entry(employee).State = EntityState.Modified;

			try
			{
				await _context.SaveChangesAsync();
			}
			catch (DbUpdateConcurrencyException)
			{
				if (!EmployeeExists(id))
				{
					return NotFound();
				}
				else
				{
					throw;
				}
			}

			return NoContent();
		}

		// POST api/Employees
		[HttpPost]
		public async Task<ActionResult<Employee>> PostEmployee([FromBody] EmployeeDTO dto)
		{
			Employee employee = new Employee(dto.FirstName, dto.LastName);

			_context.Employees.Add(employee);
			await _context.SaveChangesAsync();

			return CreatedAtRoute("GetEmployee", new { id = employee.Id }, employee);
		}

		// DELETE: api/Employees/3
		[HttpDelete("{id}")]
		public async Task<ActionResult<Employee>> DeleteEmployee(int id)
		{
			var employee = await _context.Employees.FindAsync(id);
			if (employee == null)
			{
				return NotFound();
			}

			_context.Employees.Remove(employee);
			await _context.SaveChangesAsync();

			return employee;
		}

		private bool EmployeeExists(int id)
		{
			return _context.Employees.Any(e => e.Id == id);
		}
	}
}

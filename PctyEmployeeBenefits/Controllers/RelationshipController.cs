﻿using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PctyEmployeeBenefits.Data;
using PctyEmployeeBenefits.Models;

namespace PctyEmployeeBenefits.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class RelationshipController : ControllerBase
	{
		private readonly BenefitsContext _context;

		public RelationshipController(BenefitsContext context)
		{
			_context = context;
		}

		// GET: api/Relationships
		[HttpGet]
		public async Task<ActionResult<List<Relationship>>> GetRelationships()
		{
			return await _context.Relationships.ToListAsync();
		}
	}
}

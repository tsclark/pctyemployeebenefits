﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PctyEmployeeBenefits.Data;
using PctyEmployeeBenefits.Models;
using PctyEmployeeBenefits.Models.Translators;

namespace PctyEmployeeBenefits.Controllers
{
	[Route("api/[controller]")]
	[ApiController]
	public class DependentsController : ControllerBase
	{
	private readonly BenefitsContext _context;
	private readonly IDependentTranslator _dependentTranslator;

	public DependentsController(BenefitsContext context, IDependentTranslator dependentTranslator)
	{
		_context = context;
		_dependentTranslator = dependentTranslator;
	}

	// GET: api/Dependents
	[HttpGet]
	public async Task<ActionResult<List<DependentDTO>>> GetDependents()
	{
		return await _context.Dependents
		.Include(d => d.Relationship)
		.Select(d => _dependentTranslator.toDTO(d)).ToListAsync();
	}

	// GET: api/Dependents/3
	[HttpGet("{id}", Name ="GetDependent")]
	public async Task<ActionResult<DependentDTO>> GetDependent(int id)
	{
		var dependent = await _context.Dependents
		.Include(d => d.Relationship)
		.Select(d => _dependentTranslator.toDTO(d)).FirstOrDefaultAsync(e => e.Id == id);

		if (dependent == null)
		{
			return NotFound();
		}

		return dependent;
	}

	// PUT: api/Dependents/6
	[HttpPut("{id}")]
	public async Task<IActionResult> PutDependent(int id, DependentDTO dto)
	{
		if (id != dto.Id)
		{
			return BadRequest();
		}

		Dependent dependent = await _context.Dependents.FindAsync(id);
		dependent.Update(dto.FirstName, dto.LastName, dto.RelationshipId);

		_context.Entry(dependent).State = EntityState.Modified;

		try
		{
			await _context.SaveChangesAsync();
		}
		catch (DbUpdateConcurrencyException)
		{
			if (!DependentExists(id))
			{
				return NotFound();
			}
			else
			{
				throw;
			}
		}

		return NoContent();
	}

	// POST api/Dependents
	[HttpPost]
	[ProducesResponseType(StatusCodes.Status201Created)]
	public async Task<IActionResult> PostDependent(DependentDTO dto)
	{
		Dependent dependent = new Dependent(dto.FirstName, dto.LastName, dto.RelationshipId, dto.EmployeeId);

		_context.Dependents.Add(dependent);
		await _context.SaveChangesAsync();

		return CreatedAtRoute("GetDependent", new { id = dependent.Id }, dependent);
	}

	// DELETE: api/Dependents/6
	[HttpDelete("{id}")]
	public async Task<ActionResult<Dependent>> DeleteDependent(int id)
	{
		var dependent = await _context.Dependents.FindAsync(id);
		if (dependent == null)
		{
			return NotFound();
		}

		_context.Dependents.Remove(dependent);
		await _context.SaveChangesAsync();

		return dependent;
	}

	private bool DependentExists(int id)
	{
		return _context.Dependents.Any(d => d.Id == id);
	}
	}
}

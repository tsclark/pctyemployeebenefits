﻿export const dollarFormat = (amount) => {
  const formatter = new Intl.NumberFormat('en-US', {
    style: 'currency',
    currency: 'USD',
  });
  return formatter.format(amount);
}

export const numberFormat = (number, decimals = 2) => {
  return number.toFixed(decimals);
}

export const discountFormat = (amount) => {
  if (amount == 0) {
    return 'N/A';
  }
  else {
    return dollarFormat(amount);
  }
}
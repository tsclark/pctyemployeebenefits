﻿import { 
  BENEFITSDISCOUNTPCT,
  BENEFITSDISCOUNTCONDITION,
  EMPLOYEECOSTPERPERIOD,
  DEPENDENTCOSTPERPERIOD
} from './constants';


export const colTotal = (arr, col) => {
  return arr.reduce((tot, val) => tot + val[col], 0);
};

const applyMemberDiscount = (member, cost) => {
    let memberDiscount = 0, memberDeduction = cost;
    // member is discounted where "name starts with an ‘A’"
    const { firstName, lastName } = member;
    const discountLetter = BENEFITSDISCOUNTCONDITION.toLowerCase();
    // we'll assume either first or last name, since it's not specified
    if (firstName.toLowerCase().startsWith(discountLetter) || lastName.toLowerCase().startsWith(discountLetter)) {
        memberDiscount = cost * BENEFITSDISCOUNTPCT;
        memberDeduction = cost - memberDiscount;
    } else {
    }
    return { memberDeduction, memberDiscount };
}

export const getEmployeeCost = employee => {
  const { 
    memberDeduction: employeeDeduction, 
    memberDiscount: employeeDiscount 
  } = applyMemberDiscount(employee, EMPLOYEECOSTPERPERIOD);
  return { employeeDeduction, employeeDiscount };
}
export const getDependentCost = dependent => {
  const { 
    memberDeduction: dependentDeduction, 
    memberDiscount: dependentDiscount 
  } = applyMemberDiscount(dependent, DEPENDENTCOSTPERPERIOD);
  return { dependentDeduction, dependentDiscount };
}

export const getBenefitsSummary = employee => {
  const { employeeDeduction, employeeDiscount } = getEmployeeCost(employee);
  let totalDeduction = employeeDeduction;
  let totalDiscount = employeeDiscount;

  const dependentCount = employee.dependents.length;
  employee.dependents.forEach(dependent => {
    const { dependentDeduction, dependentDiscount } = getDependentCost(dependent);
    
    totalDeduction += dependentDeduction;
    totalDiscount += dependentDiscount;
  });
  
  return { 
    deduction: totalDeduction, 
    discount: totalDiscount,
    dependentCount: dependentCount
  };
};
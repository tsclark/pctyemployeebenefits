﻿import { annualSalary, numPayPeriods, benefitsCost, benefitsDiscount } from '../payroll.config.json';

const { costPerEmployee, costPerDependent } = benefitsCost;
const { discountPct, discountCondition } = benefitsDiscount;

export const NUMPAYPERIODS = numPayPeriods;
export const BENEFITSDISCOUNTPCT = discountPct;
export const BENEFITSDISCOUNTCONDITION = discountCondition;
/* ANNUAL figures */
export const ANNUALSALARY = annualSalary;
export const BENEFITSCOSTPEREMPLOYEE = costPerEmployee;
export const BENEFITSCOSTPERDEPENDENT = costPerDependent;
/* PER PERIOD */
export const EMPLOYEECOSTPERPERIOD = BENEFITSCOSTPEREMPLOYEE / NUMPAYPERIODS;
export const DEPENDENTCOSTPERPERIOD = BENEFITSCOSTPERDEPENDENT / NUMPAYPERIODS;

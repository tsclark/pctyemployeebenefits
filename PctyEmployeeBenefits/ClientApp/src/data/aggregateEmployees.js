﻿import { getBenefitsSummary } from '../shared/utilities';
import * as payrollConfig from '../shared/payroll.config.json';

const { annualSalary, numPayPeriods } = payrollConfig;
const payPerPeriod = annualSalary / numPayPeriods;

const aggregateEmployees = data => {
  return data.map(employee => {
    const { id, firstName, lastName } = employee;
    const { deduction, discount, dependentCount } = getBenefitsSummary(employee);
    const net = payPerPeriod - deduction;

    return {
      id: id,
      firstName: firstName,
      lastName: lastName,
      dependentCount: dependentCount,
      discount: discount,
      gross: payPerPeriod,
      deduction: deduction,
      net: net
    };
  });
};

export default aggregateEmployees;

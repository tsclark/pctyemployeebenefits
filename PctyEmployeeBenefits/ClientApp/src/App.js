import React, { Component } from 'react';
import { Route, Switch } from 'react-router-dom';
import { Layout } from './components/Layout';
import Dashboard from './components/Dashboard';
import EmployeeAdd from './components/employees/EmployeeAdd';
import EmployeeView from './components/employees/EmployeeView';

import './custom.css'

export default class App extends Component {
  static displayName = App.name;

  render () {
    return (
      <Layout>
        <Switch>
          <Route exact path='/' component={Dashboard} />
          <Route path="/add" component={EmployeeAdd} />
          <Route path="/view/:id" component={EmployeeView} />
        </Switch>
      </Layout>
    );
  }
}

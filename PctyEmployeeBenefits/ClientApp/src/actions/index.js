import server from '../api/server';

export const getEmployees = () => (async () => {
  return await server.get('/employees');
})();

export const getEmployee = id => (async () => {
  return await server.get(`/employees/${id}`);
})();

export const updateEmployee = (id, formValues) => (async () => {
  return await server.put(`/employees/${id}`, JSON.stringify(formValues),
    { headers: { 'Content-Type': 'application/json' } });
})();

export const addEmployee = formValues => (async () => {
  return await server.post('/employees', JSON.stringify(formValues),
    { headers: { 'Content-Type': 'application/json' } });
})();

export const deleteEmployee = id => (async () => {
  return await server.delete(`/employees/${id}`);
})();

export const getRelationships = () => (async () => {
  return await server.get('/relationships');
})();
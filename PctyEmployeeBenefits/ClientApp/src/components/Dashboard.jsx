﻿import React, { useState, useEffect } from 'react';
import { Col, Row } from 'reactstrap';
import CostBreakdown from './widgets/CostBreakdown';
import BenefitBreakdown from './widgets/BenefitBreakdown';
import EmployeeList from './widgets/EmployeeList';
import { getEmployees } from '../actions';
import aggregateEmployees from '../data/aggregateEmployees';

const Dashboard = () => {
  const [data, setData] = useState([]);
  
  useEffect(() => {
    getEmployees().then((response) => {
      const { data } = response;
      const dataView = aggregateEmployees(data)
      setData(dataView);      
    });
  }, []);
  
  return (
    <Row id="bodyRow" className="dashboard">
      <Col xl="6">
        <CostBreakdown data={data} />
        <BenefitBreakdown data={data} />
      </Col>
      <Col xl="6">
        <EmployeeList data={data} />
      </Col>
    </Row>
  );
};

export default Dashboard;
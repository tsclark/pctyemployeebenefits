﻿import React, { Fragment, useState, useEffect } from 'react';
import { Link } from 'react-router-dom';
import { colTotal, dollarFormat, discountFormat } from '../../shared/utilities';

const EmployeeList = props => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const { data } = props;
    setData(data);
  }, [props]);

  return (
    <Fragment>
      <div className="title">Employees</div>
      <table className="table table-striped employees list benefits">
        <caption>Employee count: {data.length}</caption>
        <thead className="thead-light">
          <tr>
            <th scope="col" colSpan="2">Name</th>
            <th scope="col" className="number">Family</th>
            <th scope="col" className="currency">Discount</th>
            <th scope="col" className="currency">Benefits</th>
            <th scope="col" className="currency">Net Pay</th>
          </tr>
        </thead>
        <tbody>
        {data.map(employee => {
          const { id, firstName, lastName, dependentCount, discount, deduction, net } = employee;
          
          return (
            <tr key={id}>
              <th scope="row" className="employee"><Link to={`/view/${id}`}>{firstName}</Link></th>
              <th scope="row" className="employee"><Link to={`/view/${id}`}>{lastName}</Link></th>
              <td className="dependentCount number">{dependentCount}</td>
              <td className="discount currency">{discountFormat(discount)}</td>
              <td className="deduction currency">{dollarFormat(deduction)}</td>
              <td className="net currency">{dollarFormat(net)}</td>
            </tr>
          );
        })}
        </tbody>
        <tfoot>
          <tr className="totals">
            <th scope="col" colSpan="3" className="dependentCount number">{colTotal(data, 'dependentCount')}</th>
            <th scope="col" className="discounts currency">{dollarFormat(colTotal(data, 'discount'))}</th>
            <th scope="col" className="deduction currency">{dollarFormat(colTotal(data, 'deduction'))}</th>
            <th scope="col" className="net currency">{dollarFormat(colTotal(data, 'net'))}</th>
          </tr>
        </tfoot>
      </table>
    </Fragment>
  );
};

export default EmployeeList;
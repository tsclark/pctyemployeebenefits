﻿import React, { Fragment, useState, useEffect } from 'react';
import { Col, Row } from 'reactstrap';
import InfoBox from './InfoBox';
import { Doughnut } from 'react-chartjs-2';
import { colTotal, numberFormat } from '../../shared/utilities';

const CostBreakdown = props => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const { data } = props;
    setData(data);
  }, [props]);
  
  const gross = colTotal(data, 'gross');
  const net = colTotal(data, 'net');
  const deduction = colTotal(data, 'deduction');
  
  const config = {
    data: {
      labels: [
        'Net Pay',
        'Benefits'
      ],
      datasets: [{
        data: [ 
          Math.round(net), 
          Math.round(deduction) 
        ],
        backgroundColor: [ '#ff8f1c', '#999' ]
      }]
    },
    options: {
      legend: {
        display: false
      }
    }
  }
  
  return (
    <div className="widget">
      <Row noGutters={true}>
        <Col xs="6">
          <div className="title">Pay</div>
          <InfoBox 
            label="Net Pay"
            value={numberFormat(net, 0)} />
          <InfoBox 
            label="Benefits"
            value={numberFormat(deduction, 0)} />
          <div className="caption">Dollar figures are rounded</div>
        </Col>
        <Col xs="6" className="chartbox">
          <Doughnut 
            data={config.data} 
            options={config.options} />
        </Col>
      </Row>
    </div>
  );
};

export default CostBreakdown;
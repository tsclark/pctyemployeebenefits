﻿import React, { useState, useEffect } from 'react';
import { Col, Row } from 'reactstrap';
import InfoBox from './InfoBox';
import { Doughnut } from 'react-chartjs-2';
import { colTotal, dollarFormat, numberFormat } from '../../shared/utilities';

const BenefitBreakdown = props => {
  const [data, setData] = useState([]);

  useEffect(() => {
    const { data } = props;
    setData(data);
  }, [props]);

  const deductionTotal = colTotal(data, 'deduction');
  const discountTotal = colTotal(data, 'discount');
  const totalOfTotals = deductionTotal + discountTotal;
  
  const config = {
    data: {
      labels: [
        'Benefits',
        'Discounts'
      ],
      datasets: [{
        data: [ 
          Math.round(deductionTotal), 
          Math.round(discountTotal) 
        ],
        backgroundColor: [ '#999', '#ed2024'  ]
      }]
    },
    options: {
      legend: {
        display: false
      }
    }
  }

  return (
    <div className="widget">
      <Row noGutters={true}>
        <Col xs="6">
          <div className="title">Benefits</div>
          <InfoBox 
            label="Benefits"
            value={numberFormat(deductionTotal)} />
          <InfoBox 
            label="Discounts"
            value={numberFormat(discountTotal)} />
          <div className="caption">Total Benefits: {dollarFormat(totalOfTotals)}</div>
        </Col>
        <Col xs="6" className="chartbox">
          <Doughnut 
            data={config.data} 
            options={config.options} />
        </Col>
      </Row>
    </div>
  );
};

export default BenefitBreakdown;
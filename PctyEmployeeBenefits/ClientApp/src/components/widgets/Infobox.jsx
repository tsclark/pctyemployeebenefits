import React, { useState, useEffect } from 'react';

const convertLabelToClass = label => {
  let classname = label.toLowerCase();
  classname = classname.replace(' ', '-'); // hyphenate multiple words
  return classname;
};

const InfoBox = props => {
  const [label, setLabel] = useState("");
  const [value, setValue] = useState(-1);
  const [symbol, setSymbol] = useState("$");
  
  useEffect(() => {
    const { label, value } = props;
    setLabel(label);
    setValue(value);
    if (props.symbol) {
      setSymbol(symbol);
    }
  }, [props]);

  return (
    <div className={`infobox ${convertLabelToClass(label)}`}>
      <div className="label">{label}</div>
      <div className="value">
        <span className="symbol">{symbol}</span>
        {value}
      </div>
    </div>
  );
}

export default InfoBox;
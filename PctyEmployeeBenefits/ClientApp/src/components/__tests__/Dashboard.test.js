import React from 'react';
import { shallow } from 'enzyme';

import Dashboard from '../Dashboard';
import CostBreakdown from '../widgets/CostBreakdown';
import BenefitBreakdown from '../widgets/BenefitBreakdown';
import EmployeeList from '../widgets/EmployeeList';

describe('<Dashboard />', () => {

  it('renders <CostBreakdown /> component', () => {
    const wrapper = shallow(<Dashboard />);
    expect(wrapper.find(CostBreakdown).length).toEqual(1);
  });

  it('renders <BenefitBreakdown /> component', () => {
    const wrapper = shallow(<Dashboard />);
    expect(wrapper.find(BenefitBreakdown).length).toEqual(1);
  });

  it('renders <EmployeeList /> component', () => {
    const wrapper = shallow(<Dashboard />);
    expect(wrapper.find(EmployeeList).length).toEqual(1);
  });
});
﻿import React, { useState, useEffect } from 'react';
import { useHistory, useParams } from 'react-router-dom';
import { Col, Row, Button } from 'reactstrap';
import { getEmployee, deleteEmployee } from '../../actions';
import { 
  getEmployeeCost, 
  getBenefitsSummary, 
  discountFormat, 
  dollarFormat,
  numberFormat 
} from '../../shared/utilities';

import DependentList from './DependentList';
import InfoBox from '../widgets/InfoBox';
import ConfirmModal from '../../shared/ConfirmModal';

const EmployeeView = () => {
  const history = useHistory();
  const { id } = useParams();
  const [employee, setEmployee] = useState(null);
  const [editing, setEditing] = useState(false);
  const [modal, setModal] = useState(false);
  
  useEffect(() => {
    getEmployee(id).then((response) => {
      setEmployee(response.data);      
    });
  }, [id]);
  
  if (!employee) {
    return (
      <div className="frame">Loading...</div>
    );
  }

  const onEdit = event => {
    setEditing(!editing);
  };
  const onDelete = event => {
    setModal(true);
  };
  const onConfirmDelete = event => {
    setModal(false);
    deleteEmployee(id).then((response) => {
      history.push('/');
    })
  };
  
  const { firstName, lastName } = employee;
  const { employeeDeduction, employeeDiscount } = getEmployeeCost(employee);
  const { deduction, discount } = getBenefitsSummary(employee);

  return (
    <div className="frame employee single">
      <Row>
        <Col sm="6">
          <h2>{firstName} {lastName}</h2>
          <div className="deduction">Employee benefit cost: {dollarFormat(employeeDeduction)}</div>
          <div className="discount">Discount: {discountFormat(employeeDiscount)}</div>
        </Col>
        <Col xs={{ size: 8, offset: 4 }} sm={{ size: 6, offset: 0}} lg="4">
          <InfoBox 
            label="Benefits"
            value={numberFormat(deduction)} />
          <InfoBox 
            label="Discounts"
            value={numberFormat(discount)} />
        </Col>
      </Row>
      <Row>
        <Col lg="6">
          <DependentList employee={employee} editing={editing} />
        </Col>
      </Row>
      
      <div className="container buttons">
        {editing && (
          <Button color="danger" onClick={onDelete}>Delete</Button>
        )}
        <Button color="warning" onClick={onEdit}>Edit</Button>
      </div>

      <ConfirmModal
        modal={modal}
        setModal={setModal}
        title="Confirm Deletion"
        message={`Click 'Confirm' to permanently delete employee ${firstName} ${lastName} or 'Cancel' to go back.`}
        onConfirm={onConfirmDelete}
      />
    </div>
  );
}

export default EmployeeView;
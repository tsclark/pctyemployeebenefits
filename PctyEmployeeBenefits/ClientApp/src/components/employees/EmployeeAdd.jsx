﻿import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, Form, FormGroup, Label, Input, FormFeedback } from 'reactstrap';
import { Row, Col } from 'reactstrap';
import { addEmployee } from '../../actions';

const EmployeeAdd = () => {
  const history = useHistory();
  const [firstName, setFirstName] = useState("");
  const [lastName, setLastName] = useState("");
  const [validate, setValidate] = useState(false);

  const updateFirstName = event => {
    const { value } = event.target;
    setFirstName(value);
  };
  const updateLastName = event => {
    const { value } = event.target;
    setLastName(value);
  };
  
  const onSave = event => {
    event.preventDefault();
    setValidate(true);

    if (firstName && lastName) {
      addEmployee({ firstName, lastName }).then((response) => {
        const { id } = response.data;
        // view added employee
        history.push(`/view/${id}`);
      });
    }
  };
  
  return (
    <Form className="frame" onSubmit={onSave}>
      <h2>Add Employee</h2>
      <Row>
        <Col md={6}>
          <FormGroup>
            <Label for="firstName">First Name</Label>
            <Input type="text" 
              name="firstName" 
              id="firstName" 
              value={firstName}
              invalid={validate && !firstName}
              onChange={updateFirstName} />
            <FormFeedback>Please enter a first name</FormFeedback>
          </FormGroup>
        </Col>
        <Col md={6}>
          <FormGroup>
            <Label for="lastName">Last Name</Label>
            <Input type="text" 
              name="lastName" 
              id="lastName" 
              value={lastName}
              invalid={validate && !lastName}
              onChange={updateLastName} />
            <FormFeedback>Please enter a last name</FormFeedback>
          </FormGroup>
        </Col>
      </Row>
      <Button type="submit" color="primary">Save</Button>
    </Form>
  );
}

export default EmployeeAdd;
﻿import React, { Fragment } from 'react';
import { getDependentCost, dollarFormat, discountFormat } from '../../shared/utilities';

const DependentList = (props) => {
  const { dependents } = props.employee;
  let counter = 0;
  
  return (
    <Fragment>
      {/* <div className="title tableTitle">Dependents</div> */}
      <table className="table table-striped table-sm bordered dependents">
        <caption>{dependents.length} dependent{dependents.length === 1 ? "" : "s"}</caption>
        {dependents.length > 0 && (
          <thead>
            <tr>
              <th>Dependent</th>
              <th>Type</th>
              <th>Cost</th>
              <th>Discount</th>
            </tr>
          </thead>
        )}
        <tbody>
        {dependents.map(member => {
          counter += 1;
          const { firstName, lastName, relationship } = member;
          const { dependentDeduction, dependentDiscount } = getDependentCost(member);
          return (
            <tr key={counter}>
              <th scope="row" className="dependent">{firstName} {lastName}</th>
              <td className="relationship">{relationship}</td>
              <td className="deduction">{dollarFormat(dependentDeduction)}</td>
              <td className="discount">{discountFormat(dependentDiscount)}</td>
            </tr>
          );
        })}
        </tbody>
      </table>
    </Fragment>
    
  );
}

export default DependentList;
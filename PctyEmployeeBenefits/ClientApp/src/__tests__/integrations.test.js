import sinon from 'sinon';
import moxios from 'moxios';
import server from '../api/server';
import { equal } from 'assert';

describe('<Dashboard />', () => {
  beforeEach(() => {
    moxios.install(server);
    moxios.stubRequest('/employees', {
      status: 200,
      response: [
        { id: 1, firstName: 'John', lastName: 'Smith', dependents: [] },
        { id: 2, firstName: 'Jane', lastName: 'Sampson', dependents: [] }
      ]
    });
  });
  
  afterEach(() => {
    moxios.uninstall(server);
  });

  it('can get employees', (done) => {
    let onFulfilled = sinon.spy();
    server.get('/employees').then(onFulfilled);
    
    moxios.wait(function() {
      equal(onFulfilled.getCall(0).args[0].data.length, 2);
      done();
    });
  });
});
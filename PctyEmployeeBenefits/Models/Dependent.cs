﻿using System;

namespace PctyEmployeeBenefits.Models
{
	public class Dependent : Person
	{
		public int RelationshipId { get; private set; }
		public int EmployeeId { get; private set; }

		public virtual Relationship Relationship { get; }

		protected Dependent() { }

		public Dependent(string firstName, string lastName, int relationshipId, int employeeId)
			: base(firstName, lastName)
		{
			this.RelationshipId = relationshipId;
			this.EmployeeId = employeeId;
		}

		public void Update(string firstName, string lastName, int relationshipId)
		{
			base.UpdateName(firstName, lastName);
			RelationshipId = relationshipId;
		}
	}
}

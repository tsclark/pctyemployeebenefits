﻿using System;
using System.Collections.Generic;

namespace PctyEmployeeBenefits.Models
{
	public class Employee : Person
	{
		public virtual ICollection<Dependent> Dependents { get; private set; }

		protected Employee() { }
	
		public Employee(string firstName, string lastName)
			: base(firstName, lastName)
		{
		}

		public void Update(string firstName, string lastName)
		{
			base.UpdateName(firstName, lastName);
		}
	}
}

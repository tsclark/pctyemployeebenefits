﻿using System;
using System.Collections.Generic;

namespace PctyEmployeeBenefits.Models
{
	public class EmployeeDTO
	{
		public int Id { get; set; }
		public string FirstName { get; set; }
		public string LastName { get; set; }

		public virtual List<DependentDTO> Dependents { get; set; }
	}
}

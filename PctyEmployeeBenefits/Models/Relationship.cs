﻿namespace PctyEmployeeBenefits.Models
{
	public class Relationship : Entity
	{
		public string Label { get; private set; }

		protected Relationship() { }

		public Relationship(string label)
			: this()
		{
			Label = label;
		}
	}
}

﻿namespace PctyEmployeeBenefits.Models.Translators
{
	public interface IDependentTranslator
	{
		DependentDTO toDTO(Dependent ent);
	}

	public class DependentTranslator: IDependentTranslator
	{
		public DependentDTO toDTO(Dependent ent)
		{
			return new DependentDTO()
			{
					Id = ent.Id,
					FirstName = ent.FirstName,
					LastName = ent.LastName,
					Relationship = ent.Relationship.Label,
					RelationshipId = ent.RelationshipId,
					EmployeeId = ent.EmployeeId
			};

		}
	}
}

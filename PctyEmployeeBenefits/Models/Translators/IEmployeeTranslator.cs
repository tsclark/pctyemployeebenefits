﻿using System.Linq;

namespace PctyEmployeeBenefits.Models.Translators
{
	public interface IEmployeeTranslator
	{
		EmployeeDTO toDTO(Employee e, IDependentTranslator dependentTranslator);
	}

	public class EmployeeTranslator: IEmployeeTranslator
	{
		public EmployeeDTO toDTO(Employee e, IDependentTranslator dependentTranslator)
		{
			return new EmployeeDTO()
			{
				Id = e.Id,
				FirstName = e.FirstName,
				LastName = e.LastName,
				Dependents = e.Dependents.Select(d => dependentTranslator.toDTO(d)).ToList()
			};
		}
	}
}

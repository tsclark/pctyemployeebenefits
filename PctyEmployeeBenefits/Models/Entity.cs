﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PctyEmployeeBenefits.Models
{
	public abstract class Entity
	{
		public int Id { get; private set; }

		protected Entity() { }

		protected Entity(int id) 
			: this()
		{
			Id = id;
		}
	}
}

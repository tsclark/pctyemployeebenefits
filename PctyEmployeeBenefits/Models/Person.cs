﻿namespace PctyEmployeeBenefits.Models
{
	public abstract class Person : Entity
	{
		public string FirstName { get; private set; }
		public string LastName { get; private set; }

		protected Person()
		{
		}

		protected Person(string firstName, string lastName)
			: this()
		{
			FirstName = firstName;
			LastName = lastName;
		}

		protected void UpdateName(string firstName, string lastName)
		{
			FirstName = firstName;
			LastName = lastName;
		}
	}
}

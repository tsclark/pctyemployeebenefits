﻿using Microsoft.EntityFrameworkCore;
using PctyEmployeeBenefits.Models;

namespace PctyEmployeeBenefits.Data
{
	public class BenefitsContext : DbContext
	{
		public BenefitsContext()
		{
		}

		public virtual DbSet<Employee> Employees { get; set; }
		public virtual DbSet<Dependent> Dependents { get; set; }
		public virtual DbSet<Relationship> Relationships { get; set; }

		protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
		{
			if (!optionsBuilder.IsConfigured)
			{
				optionsBuilder.UseSqlite("Data Source=PctyBenefits.db");
			}
		}
		protected override void OnModelCreating(ModelBuilder modelBuilder)
		{
			modelBuilder.Entity<Dependent>().HasOne(d => d.Relationship);
			modelBuilder.Entity<Employee>().HasMany(e => e.Dependents);
		}
	}
}

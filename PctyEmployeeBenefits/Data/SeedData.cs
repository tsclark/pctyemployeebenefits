﻿using System.Linq;
using PctyEmployeeBenefits.Data;
using Microsoft.EntityFrameworkCore;

namespace PctyEmployeeBenefits.Models
{
	public class SeedData
	{
		public static void Initialize()
		{
			using (var context = new BenefitsContext())
			{
				context.Database.Migrate();

				if (!context.Employees.Any()) // table already seeded
				{
					context.Employees.AddRange(
					new Employee("Alexander", "Adams"),
					new Employee("Colin", "Lin"),
					new Employee("Victoria", "Lee"),
					new Employee("Samantha", "Nichols"),
					new Employee("Odette", "Yustman")
					);

					context.SaveChanges();
				}

				if (!context.Relationships.Any())
				{
					context.Relationships.AddRange(
					new Relationship("Child"),
					new Relationship("Partner"),
					new Relationship("Spouse")
					);

					context.SaveChanges();
				}

				if (!context.Dependents.Any())
				{
					context.Dependents.AddRange(
					// new Dependent(firstName, lastName, relationshipId, employeeId)
					new Dependent("Brenda", "Adams", 3, 1),
					new Dependent("Susan", "Lin", 3, 2),
					new Dependent("Jordan", "Lin", 1, 2),
					new Dependent("Jayson", "James", 2, 3),
					new Dependent("Brett", "Nichols", 3, 4),
					new Dependent("Adam", "Nichols", 1, 4),
					new Dependent("Amanda", "Nichols", 1, 4),
					new Dependent("Aaron", "Nichols", 1, 4)
					);

					context.SaveChanges();
				}
			}
		}
	}
}
